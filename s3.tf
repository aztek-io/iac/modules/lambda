resource "aws_s3_bucket_object" "this" {
  count  = local.archive_create_count
  bucket = var.s3["bucket"]
  key    = var.s3["key"]
  source = local.archive_path
}

resource "aws_s3_bucket_notification" "this" {
  for_each = toset(var.s3_triggers)
  bucket   = split(
    ":",
    each.value
  )[length(split(":", each.value)) - 1]

  lambda_function {
    lambda_function_arn = aws_lambda_function.this.arn
    events              = [
      "s3:ObjectCreated:*"
    ]
  }
}
