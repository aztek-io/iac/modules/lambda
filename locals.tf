locals {
  name = length(var.name) > 0 ? var.name : random_uuid.this.result

  default_tags = {
    Name = local.name
  }

  tags = merge(var.tags, local.default_tags)

  archive_path         = "${path.module}/${local.name}.zip"
  archive_create_count = var.archive["create"] ? 1 : 0

  etag   = fileexists(local.archive_path) ? md5(filebase64(local.archive_path)) : ""
}
