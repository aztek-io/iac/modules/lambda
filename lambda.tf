data "archive_file" "this" {
  count       = local.archive_create_count
  type        = "zip"
  source_file = var.archive["local_path"]
  output_path = local.archive_path
}

resource "aws_lambda_function" "this" {
  depends_on      = [
    data.archive_file.this
  ]

  s3_bucket = var.s3["bucket"]
  s3_key    = var.archive["create"] ? aws_s3_bucket_object.this[0].key : var.s3["key"]

  function_name = local.name
  role          = aws_iam_role.this.arn
  handler       = var.handler
  runtime       = var.runtime
  memory_size   = var.memory_size
  timeout       = var.timeout

  dynamic "environment" {
    for_each = length(var.variables) > 0 ? [""]  : []
    content {
      variables = var.variables
    }
  }

  tags = local.tags
}

resource "aws_lambda_permission" "s3_trigger" {
  for_each      = toset(var.s3_triggers)
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.arn
  principal     = "s3.amazonaws.com"
  source_arn    = each.value
}
