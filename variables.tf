variable "archive" {
  default = {
    create     = false
    local_path = "/path/to/lambda.py"
  }
}

variable "handler" {
  default = "app.handler"
}

variable "managed_policy_arns" {
  type    = list(string)
  default = []
}

variable "memory_size" {
  default = 128
}

variable "name" {
  default = ""
}

variable "policy" {
  default = ""
}

variable "runtime" {
  default = "python3.8"
}

variable "s3" {
  type    = map(string)
  default = {
    bucket = ""
    key    = ""
  }
}

variable "s3_triggers" {
  default = []
}

variable "tags" {
  default = {}
}

variable "timeout" {
  default = 15
}

variable "variables" {
  type    = map(string)
  default = {}
}
