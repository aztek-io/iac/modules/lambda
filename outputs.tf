output "archive" {
  value = join("/", list("s3:", "", var.s3["bucket"], var.s3["key"]))
}

output "name" {
  value = local.name
}

output "tags" {
  value = local.tags
}
