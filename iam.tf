data "aws_iam_policy_document" "this" {
  statement {
    sid = "LambdaAssumeRole"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "this" {
  name_prefix        = substr(local.name, 0, 31)
  assume_role_policy = data.aws_iam_policy_document.this.json
}

resource "aws_iam_role_policy_attachment" "this_managed_policies" {
  count      = length(var.managed_policy_arns)
  role       = aws_iam_role.this.name
  policy_arn = element(var.managed_policy_arns, count.index)
}

resource "aws_iam_role_policy" "this" {
  count  = length(var.policy) > 0 ? 1 : 0
  role   = aws_iam_role.this.id
  policy = var.policy
}

resource "aws_iam_role_policy" "logs" {
  role   = aws_iam_role.this.id
  policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:DescribeLogStreams"
        ],
        "Resource": [
          "arn:aws:logs:*:*:*"
        ]
      }
    ]
  }
  POLICY
}
