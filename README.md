# Module: lambda

Creates a lambda function in AWS.

## Example Usage

How to create a lambda function using this module:

```hcl
module "my_lambda" {
  source = "git@gitlab.com:aztek-io/iac/modules/lambda.git?ref=v0.1.1"
  s3     = {
    bucket = "mybucket"
    key    = "something.zip"
  }
}
```

```hcl
module "my_other_lambda" {
  source  = "git@gitlab.com:aztek-io/iac/modules/lambda.git?ref=v0.1.1"
  archive = {
    create     = true
    local_path = "/path/to/lambda.py"
  }
  s3 = {
    bucket = "mybucket"
    key    = "something.zip"
  }
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

* `s3` - (Optional|map(string)) Information about the archive is stored (or will be depending on `var.archive``) in s3. (Defaults to a map of empty strings)

### Optional Attributes

* `archive`             - (Optional|map()) Information about the archive for the lambda function. (Defaults to a map of empty strings)
* `handler`             - (Optional|string) The function entrypoint in your code. (Defaults to `app.handler`)
* `managed_policy_arns` - (Optional|list(string)) Policies to attach to the IAM role for the lambda function. (Defaults to an empty list)
* `memory_size`         - (Optional|string) Amount of memory in MB your Lambda Function can use at runtime. (Defaults to `128`)
* `name`                - (Optional|string) A unique name for your Lambda Function. (Defaults to a random uuid via `local.name`)
* `policy`              - (Optional|string)  The inline policy document. This is a JSON formatted string. (Defaults to an empty string)
* `runtime`             - (Optional|string) See [Runtimes](https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime) for valid values. (Defaults to `python3.8`)
* `s3_triggers`         - (Optional|list(string)) A list of s3 arns if an object event should trigger this lambda. (Defaults to an empty list)
* `tags`                - (Optional|map(string)) A map of tags to assign to the object. (Defaults to a map with the Name tag always set via `local.tags`)
* `timeout`             - (Optional|bool) The amount of time your Lambda Function has to run in seconds. (Defaults to `15`)
* `variables`           - (Optional|string) A map that defines environment variables for the Lambda function. (Defaults to an empty map)

## Attribute Reference

* `archive` - Displays the archive path in s3.
* `name`    - Displays the function name.
* `tags`    - Displays the resouce tags.
